setwd("../")

pkgTest("ggplot2")
pkgTest("plyr")

custdata=read.table('data/Arsteel_Customer_data.csv')
testdata=read.table('data/Arsteel_Transaction_data.csv')
testdata$gradeval=as.numeric.factor(revalue(testdata$grade, c('A'=120,'B'=110,'C'=100,'D'=95,'E'=90)))
testdata$finitval=as.numeric.factor(revalue(testdata$finition, c('A'=110,'B'=106,'C'=100,'D'=98,'E'=93)))
colnames(testdata) <- gsub("custid","id",colnames(testdata))

head(newquality)

newquality=merge(custdata,testdata,by="id",all=TRUE)

newquality$custWtP_grade<-array(0,c(dim(newquality)[1],1))
newquality$custWtP_grade[(newquality[,8]==120)]<-130
newquality$custWtP_grade[(newquality[,8]==110)]<-115
newquality$custWtP_grade[(newquality[,8]==100)]<-100
newquality$custWtP_grade[(newquality[,8]==95)]<-97
newquality$custWtP_grade[(newquality[,8]==90)]<-93

#create a new column:customer willing to pay for finit on base price
newquality$custWtP_fin<-array(0,c(dim(newquality)[1],1))
newquality$custWtP_fin[(newquality[,9]==110)]<-115
newquality$custWtP_fin[(newquality[,9]==106)]<-109
newquality$custWtP_fin[(newquality[,9]==100)]<-100
newquality$custWtP_fin[(newquality[,9]==98)]<-99
newquality$custWtP_fin[(newquality[,9]==93)]<-96


newquality$profit_grade<- (newquality$custWtP_grade)-(newquality$gradeval)
newquality$profit_finition<- (newquality$custWtP_fin)-(newquality$finitval)

newquality$quality<- newquality$profit_grade+newquality$profit_finition

newquality$qualitylevel<-array(0,c(dim(newquality)[1],1))
newquality$qualitylevel[(newquality$grade=="A")& newquality$finition =="A"]<- "01"
newquality$qualitylevel[(newquality$grade=="A")& newquality$finition =="B"]<- "02"
newquality$qualitylevel[(newquality$grade=="A")& newquality$finition =="C"]<- "03"
newquality$qualitylevel[(newquality$grade=="A")& newquality$finition =="D"]<- "04"
newquality$qualitylevel[(newquality$grade=="A")& newquality$finition =="E"]<- "05"
newquality$qualitylevel[(newquality$grade=="B")& newquality$finition =="A"]<- "06"
newquality$qualitylevel[(newquality$grade=="B")& newquality$finition =="B"]<- "07"
newquality$qualitylevel[(newquality$grade=="B")& newquality$finition =="C"]<- "08"
newquality$qualitylevel[(newquality$grade=="B")& newquality$finition =="D"]<- "09"
newquality$qualitylevel[(newquality$grade=="B")& newquality$finition =="E"]<- "10"
newquality$qualitylevel[(newquality$grade=="C")& newquality$finition =="A"]<- "11"
newquality$qualitylevel[(newquality$grade=="C")& newquality$finition =="B"]<- "12"
newquality$qualitylevel[(newquality$grade=="C")& newquality$finition =="C"]<- "13"
newquality$qualitylevel[(newquality$grade=="C")& newquality$finition =="D"]<- "14"
newquality$qualitylevel[(newquality$grade=="C")& newquality$finition =="E"]<- "15" 
newquality$qualitylevel[(newquality$grade=="D")& newquality$finition =="A"]<- "16"
newquality$qualitylevel[(newquality$grade=="D")& newquality$finition =="B"]<- "17"
newquality$qualitylevel[(newquality$grade=="D")& newquality$finition =="C"]<- "18"
newquality$qualitylevel[(newquality$grade=="D")& newquality$finition =="D"]<- "19"
newquality$qualitylevel[(newquality$grade=="D")& newquality$finition =="E"]<- "20"
newquality$qualitylevel[(newquality$grade=="E")& newquality$finition =="A"]<- "21"
newquality$qualitylevel[(newquality$grade=="E")& newquality$finition =="B"]<- "22"
newquality$qualitylevel[(newquality$grade=="E")& newquality$finition =="C"]<- "23"
newquality$qualitylevel[(newquality$grade=="E")& newquality$finition =="D"]<- "24"
newquality$qualitylevel[(newquality$grade=="E")& newquality$finition =="E"]<- "25"

newquality$sell <- newquality$price * newquality$weight

# ploting
qplot(data = newquality, qualitylevel)
qplot(data = newquality, x = qualitylevel, y = sell, geom = 'boxplot', fill = qualitylevel)

clust_table <- data.frame(profitability = newquality$quality, sell = newquality$sell)
head(clust_table)
d = dist(clust_table, method = "euclidean") # Compute the distance between the points.
hcward = hclust(d, method = "ward.D") # Hiearchical Clustering using Ward criterion
# plot(hcward) # dendogram
newquality$groups = cutree(hcward, k = 4)
plot(hcward) # dendogram
rect.hclust(hcward, k=4, border="red")

