# clean env
setwd("../")
rm(list = ls(all = TRUE))

# read the table and get some info
data <- read.table('data/Nile_eBooks_data.csv')
str(data)
summary(data)

# report recency and frequency to age of the customer
data$recencytolife = data$recency / data$lifetime
data$frequencytolife = data$frequency / data$lifetime

testdata = data.frame(recency = data$recencytolife, frequency = data$frequencytolife)
testdata = data.frame(scale(testdata))
d_euclid = dist(testdata, method = "euclidean")
hcward = hclust(d_euclid, method = "ward.D")

plot(hcward)
rect.hclust(hcward, k = 4, border = "red")

testdata$groups = cutree(hcward,k = 4)


# check if package is installed before loading it
pkgTest <- function(pack) {
  if (!require(pack,character.only = TRUE)) {
    install.packages(pack, dependencies = TRUE)
    if (!require(pack,character.only = TRUE))
      stop(paste(pack, "not found"))
  }
}
# Plot the clustered data
pkgTest("lattice")
xyplot(monetary ~ frequency, main = "After Clustering", type = "p",
       group = groups, data = testdata, 
       auto.key = list(title = "Group", space = "left", cex = 1.0, just = 0.95),
       par.settings = list(superpose.line = list(pch = 0:18, cex = 1))
)

# Let's come back to the orginal dataset
data$groups = cutree(hcward,k = 4) #Create segments for k=X

# Let's transform the categorical variable "socialclass" into a dummy variable for the aggregation done later
datamod = cbind(data,(model.matrix(~ socialclass - 1, data = data)))
datamod$socialclass = NULL
colnames(datamod) <- gsub("socialclass","",colnames(datamod))

# Let's do some summary statistics by segment: are all the variables relevant here?
aggdata = aggregate(. ~ groups, data = datamod, FUN = mean)
# aggdata = aggregate(
#   cbind(
#     female,age,children,fiction,nonfict,fictkids,nonfictkids,lifetime,monetary,recencytolife,frequencytolife,A,B,C,D
#   ) ~ groups, data = datamod, FUN = mean
# )
aggdata = aggregate(
  cbind(
    # female,age,children,A,B,C,D
    fiction,nonfict,fictkids,nonfictkids,lifetime,monetary,recencytolife,frequencytolife
    ) ~ groups, data = datamod, FUN = mean
)


# It's always interesting to compare the size of the resulting segments
proptemp = aggregate(id ~ groups, data = data, FUN = length)
aggdata$proportion = (proptemp$id) / sum(proptemp$id)
aggdata = aggdata[order(aggdata$proportion,decreasing = T),]
aggdata
write.table(aggdata,'data/Nile_SumStat.csv')

reg = lm(frequencytolife ~ ., data = data) 
cor(reg$fitted.values,data$frequencytolife)
plot(reg$fitted.values,data$frequencytolife)
summary(reg)

# Another regression to test: is it relevant? Are all the drivers relevant here?
reg = lm(recencytolife ~ ., data = data) # Estimate the drivers of recency
cor(reg$fitted.values,data$recencytolife) # Assess the correlation between estimated recency and actual
plot(reg$fitted.values,data$recencytolife)
summary(reg) # Report the results of the regression

# Another regression to test: is it relevant? Are all the drivers relevant here?
reg = lm(monetary ~ ., data = data) # Estimate the drivers of recency
cor(reg$fitted.values,data$monetary) # Assess the correlation between estimated recency and actual
plot(reg$fitted.values,data$monetary)
summary(reg) # Report the results of the regression


# Decision Tree
pkgTest("party")
estimgroup <- ctree(as.factor(groups) ~ .,data = data) #To estimate the tree

# estimgroup <- cforest(groups ~ .,data = data, controls = cforest_control(mtry = 2, mincriterion = 0, ntree = 20)) #To estimate the tree
# 
# party_tree <- party::prettytree(estimgroup@ensemble[[1]], names(estimgroup@responses))
# plot(party_tree)

plot(estimgroup)
print(estimgroup) 
ctree()

plot(estimgroup,type = "simple", # To have the proportion
  inner_panel = node_inner(estimgroup,pval = FALSE,# no p-values
                           id = FALSE),           # no id on nodes
  terminal_panel = node_terminal(
    estimgroup, id = FALSE,
    digits = 2,                   # few digits on numbers
    fill = c('white')
  )
)            # make box white not grey

# Assessment of the model accuracy:
my.prediction <- predict(estimgroup, data)
table(my.prediction, data$groups)# Let's look at the confusion matrix
sum(diag(table(my.prediction, data$groups))) / length(my.prediction) # % of correctly classified observation

BTYDmatrix = data[1,10:12]
BTYDmatrix = data[,10:12]
BTYDmatrix$id  <- data$id
BTYDmatrixTMP  <-  BTYDmatrix [ , c(4, 1, 2, 3)]
BTYDMatrixO  <-  BTYDmatrix[order(BTYDmatrix$id, decreasing = FALSE),]